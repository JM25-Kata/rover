#include <iostream>
#include "../src/InputParser.h"
#include "config.h"

int main() {
    std::cout << "project name: " << PROJECT_NAME << " version: " << PROJECT_VER << std::endl;
    std::cout << "Hello, World! \n";
    return 0;
}
