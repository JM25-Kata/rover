#include "InputParser.h"
#include <string>
#include <fstream>
#include <algorithm>

InputParser::InputParser(const std::string& inputFile) {
    std::ifstream file(inputFile);
    if (! file.is_open())
        throw std::exception();

    std::string line;
    while (std::getline(file, line)) {
        inputLines.push_back(line);
    }

    if (inputLines.empty()) {
        throw std::exception();
    }
    if (inputLines.size() % 2 != 1) {
        throw std::exception();
    }
}

std::pair<int, int> InputParser::readGrid() const {
    std::string line = inputLines[0];
    auto del = line.find(' ');

    if (del == std::string::npos) {
        throw std::exception();
    }

    int x, y;
    x = stoi(line.substr(0,del));
    y = stoi(line.substr(del));

    return {x, y};
}

Rover InputParser::readRover() const {
    Rover rover = getRover(inputLines[1]);

    std::vector<Instruction> instructions = translateInstructions(inputLines[2]);
    rover.instructionList = instructions;
    return rover;
}

std::vector<Instruction> InputParser::translateInstructions(const std::string& line) const {
    std::vector<Instruction> instructions;
    for(char c: line) {
        switch (c) {
            case 'L':
            case 'R':
            case 'M':
                instructions.push_back(static_cast<Instruction>(c));
            default:
                continue;
        }
    }
    return instructions;
}

Rover InputParser::getRover(const std::string& line) const {
    auto xyDel = line.find(' ');


    if (xyDel == std::string::npos) {
        throw std::exception();
    }

    int x, y;
    char o;
    x = stoi(line.substr(0,xyDel));

    auto reminder = line.substr(xyDel+1);
    auto yoDel = reminder.find(' ');
    y = stoi(reminder.substr(0, yoDel));
    o = reminder.substr(yoDel+1).front();

    return Rover(x, y, o, {});
}

RoverManager InputParser::getManager() const {
    std::vector<Rover> rovers;
    std::pair<int,int> grid = readGrid();
    for (unsigned int i = 1; i < inputLines.size(); i+=2) {
        Rover rover = getRover(inputLines[i]);
        std::vector<Instruction> instructions = translateInstructions(inputLines[i+1]);
        rover.instructionList = instructions;
        rover.bounds = grid;
        rovers.push_back(std::move(rover));
    }
    return RoverManager(grid, rovers);
}

