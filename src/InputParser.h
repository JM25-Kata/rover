#pragma once

#include <string>
#include <utility>
#include <vector>
#include "Rover.h"
#include "RoverManager.h"

class InputParser {

public:
    InputParser(const std::string& inputFile);

    std::pair<int, int> readGrid() const;
    RoverManager getManager() const;
private:

    Rover readRover() const;
    std::vector<std::string> inputLines;

    Rover getRover(const std::string& line) const;

    std::vector<Instruction> translateInstructions(const std::string& line) const;
};