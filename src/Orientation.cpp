#include "Orientation.h"

const std::list<Orientation> cardinalList = {Orientation::NORTH,
                                             Orientation::EAST,
                                             Orientation::SOUTH,
                                             Orientation::WEST};

Orientation getNextAntiClockwiseOrientation(Orientation o) {
    auto itOrientation = std::find(cardinalList.begin(), cardinalList.end(), o);
    if (itOrientation == cardinalList.begin())
        return *(cardinalList.rbegin());
    else
        return *--itOrientation;
}

Orientation getNextClockwiseOrientation(Orientation o) {
    auto itOrientation = std::find(cardinalList.begin(), cardinalList.end(), o);
    if (itOrientation == --cardinalList.end())
        return *cardinalList.begin();
    else
        return *++itOrientation;
}
