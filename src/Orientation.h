#pragma once

#include <list>

enum class Orientation: char {
    NORTH = 'N',
    SOUTH = 'S',
    EAST = 'E',
    WEST = 'W',
};

Orientation getNextClockwiseOrientation(Orientation o);
Orientation getNextAntiClockwiseOrientation(Orientation o);
