//
// Created by Archfiend on 09/12/2019.
//

#include <vector>
#include <algorithm>
#include "Rover.h"
#include "Orientation.h"

Rover::Rover(int x, int y, char orientation, std::vector<Instruction> instructions) noexcept
        : x{x}, y{y}, o{static_cast<Orientation >(orientation)}, instructionList{instructions} {

}

Rover::Rover(const Rover &other) noexcept {
    x = other.x;
    y = other.y;
    o = other.o;
    instructionList = other.instructionList;
    bounds = other.bounds;
}

std::pair<int, int> Rover::coordinates() const {
    return {x, y};
}

Orientation Rover::orientation() const {
    return o;
}

bool Rover::operator==(const Rover &other) const {
    return x == other.x &&
           y == other.y &&
           o == other.o &&
           instructionList == other.instructionList;
}

bool Rover::operator!=(const Rover &other) const {
    return !(*this == other);
}

std::vector<Instruction> Rover::instructions() {
    return instructionList;
}

void Rover::execute() {
    for (auto i: instructionList) {
        switch (i) {
            case Instruction::LEFT: {
                o = getNextAntiClockwiseOrientation(o);
            }
                break;
            case Instruction::RIGHT: {
                o = getNextClockwiseOrientation(o);
            }
                break;
            case Instruction::MOVE: {
                int oldX = x;
                int oldY = y;
                switch (orientation()) {
                    case Orientation::SOUTH:
                        y -= 1;
                        break;
                    case Orientation::WEST:
                        x -= 1;
                        break;
                    case Orientation::NORTH:
                        y += 1;
                        break;
                    case Orientation::EAST:
                        x += 1;
                        break;
                }
                if (x < 0 || x > bounds.first) x = oldX;
                if (y < 0 || y > bounds.second) y = oldY;
                break;
            }
            default:
                throw std::exception();
        }
    }
}
