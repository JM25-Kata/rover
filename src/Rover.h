//
// Created by Archfiend on 09/12/2019.
//

#pragma once


#include <utility>
#include "Orientation.h"
#include <vector>

enum class Instruction: char {
    LEFT = 'L',
    RIGHT = 'R',
    MOVE = 'M',
};

class Rover {
    friend class InputParser;
public:
    Rover(int x, int y, char orientation, std::vector<Instruction> instructions) noexcept ;
    Rover(const Rover& other) noexcept ;

    void execute();

    std::pair<int,int> coordinates() const;
    Orientation orientation() const;
    std::vector<Instruction> instructions();

    bool operator==(const Rover& other) const;
    bool operator!=(const Rover& other) const;
private:
    int x;
    int y;
    Orientation o;
    std::vector<Instruction> instructionList;
    std::pair<int,int> bounds;
};
