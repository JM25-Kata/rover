//
// Created by Archfiend on 11/12/2019.
//

#include "RoverManager.h"

#include <utility>

void RoverManager::executeNext() {
    rovers[nextExecution].execute();
    nextExecution+=1;
}

Rover RoverManager::getRover(int roverIndex) const {
    return rovers[roverIndex];
}

RoverManager::RoverManager(std::pair<int, int> pair, std::vector<Rover> vector):
grid{std::move(pair)},
rovers{std::move(vector)}
{

}

std::pair<int, int> RoverManager::getGrid() const {
    return grid;
}
