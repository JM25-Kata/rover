#pragma once

#include "Rover.h"

class RoverManager {
public:
    RoverManager(std::pair<int, int> pair, std::vector<Rover> vector);

    void executeNext();
    Rover getRover(int roverIndex) const;
    std::pair<int,int> getGrid() const;

private:
    int nextExecution{0};
    std::pair<int,int> grid;
    std::vector<Rover> rovers;
};