#include <string>
#include "gtest/gtest.h"
#include "../src/InputParser.h"
#include "../src/RoverManager.h"

TEST(TestInput, ReadGrid)
{
    InputParser ip("inputs/input10x10.txt");
    auto expectedResult = std::make_pair(10, 10);
    RoverManager rm = ip.getManager();
    EXPECT_EQ(rm.getGrid(), expectedResult);
    InputParser ip20("inputs/input20x20.txt");
    expectedResult = std::make_pair(20, 20);
    rm = ip20.getManager();
    EXPECT_EQ(rm.getGrid(), expectedResult);
}

TEST(TestInput, ReadRover)
{
    InputParser ip("inputs/rover_init.txt");
    RoverManager rm = ip.getManager();
    EXPECT_EQ(rm.getRover(0), Rover(1, 2, 'N', {}));
    EXPECT_EQ(rm.getRover(0).coordinates(), std::make_pair(1, 2));
    EXPECT_EQ(rm.getRover(0).orientation(), Orientation::NORTH);
    InputParser ipS("inputs/rover_initS.txt");
    Rover res_south = ipS.getManager().getRover(0);
    EXPECT_EQ(res_south, Rover(33, 10, 'S', {}));
    EXPECT_EQ(res_south.coordinates(), std::make_pair(33, 10));
    EXPECT_EQ(res_south.orientation(), Orientation::SOUTH);
}

TEST(TestInput, ReadInstruction)
{
    InputParser ip("inputs/rover_mov.txt");
    Rover res = ip.getManager().getRover(0);
    Rover expected(11, 22, 'O', {Instruction::LEFT, Instruction::RIGHT, Instruction::MOVE});
    std::vector<Instruction> expectedInstructions = {Instruction::LEFT, Instruction::RIGHT, Instruction::MOVE};
    EXPECT_EQ(res,expected);
    EXPECT_EQ(res.instructions(), expectedInstructions);
}

TEST(MoveRover, Move)
{
    {
        InputParser ip("inputs/input_no_mov_instruction.txt");
        Rover res = ip.getManager().getRover(0);
        res.execute();
        EXPECT_EQ(res.coordinates(), std::make_pair(0, 0));
        EXPECT_EQ(res.orientation(), Orientation::EAST);
    }
    {
        InputParser ip("inputs/input_L.txt");
        Rover res = ip.getManager().getRover(0);
        res.execute();
        EXPECT_EQ(res.coordinates(), std::make_pair(0, 0));
        EXPECT_EQ(res.orientation(), Orientation::NORTH);
    }
    {
        InputParser ip("inputs/input_R.txt");
        Rover res = ip.getManager().getRover(0);
        res.execute();
        EXPECT_EQ(res.coordinates(), std::make_pair(0, 0));
        EXPECT_EQ(res.orientation(), Orientation::SOUTH);
    }
    {
        InputParser ip("inputs/input_M.txt");
        Rover res = ip.getManager().getRover(0);
        res.execute();
        EXPECT_EQ(res.coordinates(), std::make_pair(1, 0));
        EXPECT_EQ(res.orientation(), Orientation::EAST);
    }
    {
        InputParser ip("inputs/input_multiple.txt");
        Rover res = ip.getManager().getRover(0);
        res.execute();
        EXPECT_EQ(res.coordinates(), std::make_pair(5, 5));
        EXPECT_EQ(res.orientation(), Orientation::NORTH);
    }
}

TEST(Execution, sequencial)
{
    InputParser ip("inputs/input_multirover.txt");
    RoverManager rm = ip.getManager();
    rm.executeNext();
    EXPECT_EQ(rm.getRover(0).coordinates(), std::make_pair(1,0));
    EXPECT_EQ(rm.getRover(1).coordinates(), std::make_pair(50,50));
    rm.executeNext();
    EXPECT_EQ(rm.getRover(0).coordinates(), std::make_pair(1,0));
    EXPECT_EQ(rm.getRover(1).coordinates(), std::make_pair(50,48));
}

TEST(Execution, dontMoveOutsideBounds)
{
    InputParser ip("inputs/input_outOfBounds.txt");
    RoverManager rm = ip.getManager();
    rm.executeNext();
    EXPECT_EQ(rm.getRover(0).coordinates(), std::make_pair(10,10));
}